#include <iostream>
#include "abstract_factory.hpp"

using namespace std;

int main()
{
	// Abstract factory #1
    ConcreteFactory1 factory1;
    Client client1(factory1);
    client1.run();


	// Abstract factory #2
	ConcreteFactory2 factory2;
    Client client2(factory2);
    client2.run();
}
