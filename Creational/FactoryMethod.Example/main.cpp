#include <iostream>
#include <string>
#include <fstream>
#include <cassert>
#include <map>
#include <algorithm>
#include <stdexcept>

#include "shape.hpp"

//#include "circle.hpp"
//#include "rectangle.hpp"

#include "application.hpp"
#include "shape_factory.hpp"

#include <boost/bind.hpp>

using namespace Drawing;
using namespace std;

//Shape* create_shape(const std::string& id)
//{
//	if (id == "Circle")
//		return new Circle();
//	else if (id == "Rectangle")
//		return new Rectangle();

//	throw std::runtime_error("Bad identifier");
//}

namespace Details
{
    class ShapeCreator
    {
    public:
        virtual Shape* create_shape() = 0;
        virtual ~ShapeCreator() = default;
    };

    class ShapeFactory
    {
        std::map<std::string, std::shared_ptr<ShapeCreator>> factory_;
    public:
        bool register_creator(const std::string& id, std::shared_ptr<ShapeCreator> creator)
        {
            return factory_.insert(std::make_pair(id, creator)).second;
        }

        Shape* create(const std::string& id) const
        {
            auto creator = factory_.find(id);

            if (creator != factory_.end())
                return creator->second->create_shape();

            throw std::runtime_error("Bad shape Id");
        }
    };
}



class GraphicsDocument : public Document
{
	vector<Shape*> shapes_;
	string title_;
public:
	virtual void open(const string& file_name)
	{
		title_ = file_name;

		ifstream fin(file_name.c_str());

		string type_identifier;

		cout << "Loading a file...\n";
		while(!fin.eof())
		{
			if (fin >> type_identifier)
			{
				cout << type_identifier << "\n";

                Shape* shp_ptr = ShapeFactory::instance().create_object(type_identifier);
				shp_ptr->read(fin);

				shapes_.push_back(shp_ptr);
			}
		}

		cout << "Loading finished." << endl;
	}

	virtual void save(const std::string& file_name)
	{
		ofstream fout(file_name.c_str());
		for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::write, _1, boost::ref(fout)));
		fout.close();

		title_ = file_name;
	}

	virtual string get_title() const
	{
		return title_;
	}

	void show()
	{
		cout << "\n\nDrawing:\n";
		for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::draw, _1));
	}

	~GraphicsDocument()
	{
		for(std::vector<Shape*>::iterator it = shapes_.begin(); it != shapes_.end(); ++it)
			delete *it;
	}
};

class GraphicsApplication : public Application
{
public:
	virtual Document* create_document()
	{
		return new GraphicsDocument();
	}
};



//template <typename Factory>
//void bootstrap(Factory& f)
//{
//    f.register_creator("Circle", ShapeCreator<Circle>());
//    f.register_creator("Rectangle", ShapeCreator<Rectangle>());
//}

int main()
{
	GraphicsApplication app;

    app.open_document("../drawing.txt");

    Document* ptr_doc = app.find_document("../drawing.txt");

	ptr_doc->show();

	ptr_doc->save("new_drawing.txt");
}
