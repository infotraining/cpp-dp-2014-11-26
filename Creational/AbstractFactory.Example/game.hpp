#ifndef GAME_HPP_
#define GAME_HPP_

#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>
#include <memory>
#include <cassert>

#include "enemy_factory.hpp"

namespace Game {

enum class GameLevel { easy, die_hard };

class GameApp
{
    std::vector<std::unique_ptr<Enemy>> enemies_;
    std::unique_ptr<AbstractEnemyFactory> enemy_factory_;

public:
    GameApp() : enemy_factory_{nullptr}
	{
	}

    GameApp(const GameApp&) = delete;
    GameApp& operator=(const GameApp&) = delete;

	void select_level(GameLevel level)
	{
		switch (level)
		{
            case GameLevel::easy:
                enemy_factory_.reset(new EasyLevelEnemyFactory());
				break;
            case GameLevel::die_hard:
                enemy_factory_.reset(new DieHardLevelEnemyFactory());
				break;
		}
	}

	void init_game(int number_of_enemies)
	{
        assert(enemy_factory_ != nullptr);

		for(int i = 0; i <  number_of_enemies/3; ++i)
		{
			enemies_.push_back(enemy_factory_->CreateSoldier());
			enemies_.push_back(enemy_factory_->CreateMonster());
			enemies_.push_back(enemy_factory_->CreateSuperMonster());
		}
	}

	void test_monsters()
	{
        for(const auto& e : enemies_)
            e->action();
	}
};

}
#endif /*GAME_HPP_*/
