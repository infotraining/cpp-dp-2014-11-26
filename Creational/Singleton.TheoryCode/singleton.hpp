#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

#include <iostream>
#include <mutex>
#include <atomic>

class Singleton
{
public:
    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;

	static Singleton& instance()
	{             
//        if (!instance_.load())
//        {
//            std::lock_guard<std::mutex> lk {mtx_};

//            if (!instance_.load())
//            {
//                Singleton* temp = new Singleton();
//                instance_.store(temp);
//            }
//        }

//        return *instance_;

        static Singleton unique_instance;

        return unique_instance;
	}

    void do_something();

private:
//    static std::mutex mtx_;
//    static std::atomic<Singleton*> instance_;  // uniqueInstance

    Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
	{ 
		std::cout << "Constructor of singleton" << std::endl; 
    }

    ~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
    {
        std::cout << "Singleton has been destroyed!" << std::endl;
    }
};

//std::mutex Singleton::mtx_;
//std::atomic<Singleton*> Singleton::instance_ ;

void Singleton::do_something()
{
	std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
}

#endif /*SINGLETON_HPP_*/
