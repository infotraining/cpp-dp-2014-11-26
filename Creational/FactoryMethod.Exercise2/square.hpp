#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include "shape.hpp"

// TODO: Dodać klase Square
namespace Drawing
{
    class Square : public ShapeBase
    {
        int size_;
    public:
        Square(int x = 0, int y = 0, int size = 0) : ShapeBase{x, y}, size_{size}
        {
        }

        void draw() const
        {
            std::cout << "Drawing square at: " << point() << " with size: " << size_ << std::endl;
        }

        void read(std::istream& in)
        {
            Point pt;

            in >> pt >> size_;
            set_point(pt);
        }

        void write(std::ostream& out)
        {
            out << "Square " << point() << " " << size_ << std::endl;
        }
    };

}


#endif /* SQUARE_HPP_ */
