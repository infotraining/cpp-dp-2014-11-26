#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <vector>
#include "point.hpp"

namespace Drawing
{

class Shape
{
public:
	virtual void draw() const = 0;  // rysuje obiekt na ekranie
	virtual void move(int dx, int dy) = 0;  // przesuwa figure +=dx i +=dy

	virtual void read(std::istream& in) = 0;  // wczytanie ze strumienia
	virtual void write(std::ostream& out) = 0; // zapis do strumienia

	virtual ~Shape()
	{
	}
};

template <typename ShapeType>
class ShapeCreator
{
public:
	Shape* operator()() const
	{
		return new ShapeType();
	}
};

class ShapeBase: public Shape
{
	Point location_;
public:
	ShapeBase(int x, int y) :
			location_(Point(x, y))
	{

	}

	ShapeBase(const Point& location) :
            location_(location)
	{

	}

	virtual void move(int dx, int dy)
    {
		std::cout << "Moving shape" << std::endl;

		location_.translate(dx, dy);
	}

	Point point() const
	{
		return location_;
	}
protected:
	void set_point(Point location)
	{
		location_ = location;
	}
};

}

#endif
