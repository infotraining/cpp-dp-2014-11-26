#ifndef SINGLETON_HOLDER_HPP_
#define SINGLETON_HOLDER_HPP_

#include <cassert>

namespace GenericSingleton
{

typedef void (*atexit_pfn_t)();

/////////////////////////////////////////////////////////////////////////
// klasa szablonowa CreateUsingNew
// implementacja wytycznej CreationPolicy używanej przez SingletonHolder
/////////////////////////////////////////////////////////////////////////

template <typename T>
struct CreateUsingNew
{
	static T* create()
	{
		return new T;
	}

	static void destroy(T* p)
	{
		delete p;
	}
};

/////////////////////////////////////////////////////////////////////////
// klasa CreateStatic
// implementacja wytycznej CreationPolicy używanej przez SingletonHolder
/////////////////////////////////////////////////////////////////////////
template <typename T>
struct CreateStatic
{
    static T* create()
	{
        alignas(T) static char static_memory_[sizeof(T)];
		return new(&static_memory_) T;
	}

	static void destroy(T* p)
	{
		p->~T();
	}
};

////////////////////////////////////////////////////////////////////////////////
// klasa szablonowa DefaultLifetime
// implementacja wytycznej LifetimePolicy używanej przez SingletonHolder
////////////////////////////////////////////////////////////////////////////////
template <typename T>
struct DefaultLifetime
{
	static void schedule_destruction(T*, atexit_pfn_t pFun)
	{
		std::atexit(pFun);
	}

	static void on_dead_reference()
	{
		throw std::logic_error("Dead Reference Detected");
	}
};

////////////////////////////////////////////////////////////////////////////////
// klasa szablonowa PhoenixSingleton
// implementacja wytycznej LifetimePolicy używanej przez SingletonHolder
////////////////////////////////////////////////////////////////////////////////
template <typename T>
class PhoenixSingleton
{
public:
	static void schedule_destruction(T*, atexit_pfn_t pFun)
	{
		std::atexit(pFun);
	}

	static void on_dead_reference()
	{
	}
};

////////////////////////////////////////////////////////////////////////////////
// klasa szablonowa NoDestroySingleton
// implementacja wytycznej LifetimePolicy używanej przez SingletonHolder
////////////////////////////////////////////////////////////////////////////////
template <class T>
struct NoDestroy
{
	static void schedule_destruction(T*, atexit_pfn_t pFun)
    {
    }

    static void on_dead_reference()
    {
    }
};

////////////////////////////////////////////////////////////////////////////////
// klasa szablonowa SingletonHolder
////////////////////////////////////////////////////////////////////////////////

template
<
	typename T,
	template<class> class CreationPolicy = CreateUsingNew,
	template<class> class LifetimePolicy = DefaultLifetime
>
class SingletonHolder
{
public:
    SingletonHolder(const SingletonHolder&) = delete;
    SingletonHolder& operator=(const SingletonHolder&) = delete;
	static T& instance();

private:
	static void make_instance();
	static void destroy_singleton();

	static T* ptr_instance_;
	static bool destroyed_;

	// prywatny konstruktor
    SingletonHolder() = default;
};

// dane statyczne singletonu
template
<
	class T,
    template <class> class CreationPolicy,
    template <class> class LifetimePolicy
>
T* SingletonHolder<T, CreationPolicy, LifetimePolicy>::ptr_instance_;

template
<
	class T,
    template <class> class CreationPolicy,
    template <class> class LifetimePolicy
>
bool SingletonHolder<T, CreationPolicy, LifetimePolicy>::destroyed_;

// implementacja metody SingletonHolder::instance()
template
<
	class T,
    template <class> class CreationPolicy,
    template <class> class LifetimePolicy
>
inline T& SingletonHolder<T, CreationPolicy, LifetimePolicy>::instance()
{
	if (!ptr_instance_)
        make_instance();

    return *ptr_instance_;
}

// implementacja metody SingletonHolder::make_instance()
template
<
	class T,
    template <class> class CreationPolicy,
    template <class> class LifetimePolicy
>
void SingletonHolder<T, CreationPolicy, LifetimePolicy>::make_instance()
{
	if (!ptr_instance_)
    {
		if (destroyed_)
        {
			LifetimePolicy<T>::on_dead_reference();
            destroyed_ = false;
        }

        ptr_instance_ = CreationPolicy<T>::create();
        LifetimePolicy<T>::schedule_destruction(ptr_instance_, &destroy_singleton);
    }
}

// implementacja metody SingletonHolder::destroy_singleton()
template
<
	class T,
    template <class> class CreationPolicy,
    template <class> class LifetimePolicy
>
void SingletonHolder<T, CreationPolicy, LifetimePolicy>::destroy_singleton()
{
	assert(!destroyed_);
    CreationPolicy<T>::destroy(ptr_instance_);
    ptr_instance_ = 0;
    destroyed_ = true;
}

}

#endif /* SINGLETON_HOLDER_HPP_ */
