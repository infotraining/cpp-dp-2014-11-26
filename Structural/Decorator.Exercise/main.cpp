#include <memory>
#include "coffeehell.hpp"

using namespace std;

template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(forward<Args>(args)...));
}

void use_make_unique()
{
    string desc = "Kawa";
    float price = 7.9;

    auto cf1 = make_unique<Espresso>(price, desc);

    auto cf2 = make_unique<Espresso>(price, string{"Kawa"});
}

void client(std::unique_ptr<Coffee> coffee)
{
    std::cout << "Description: " << coffee->get_description() << "; Price: " << coffee->get_total_price() << std::endl;
    coffee->prepare();
}

class CoffeeBuilder
{
    std::unique_ptr<Coffee> coffee_;
public:
    template <typename BaseCoffee>
    CoffeeBuilder& create_base()
    {
        coffee_.reset(new BaseCoffee);

        return *this;
    }

    template <typename Condiment>
    CoffeeBuilder& add()
    {
        coffee_ = make_unique<Condiment>(move(coffee_));

        return *this;
    }

    std::unique_ptr<Coffee> get_coffee()
    {
        return move(coffee_);
    }
};

int main()
{
    auto cf = make_unique<Whipped>(
                make_unique<Whisky>(
                    make_unique<Whisky>(
                        make_unique<ExtraEspresso>(
                            make_unique<Espresso>()))));

    client(move(cf));

    cout << "\n\n";

    CoffeeBuilder cb;

    cb.create_base<Espresso>();
    cb.add<ExtraEspresso>().add<Whisky>().add<Whipped>();

    client(cb.get_coffee());
}
