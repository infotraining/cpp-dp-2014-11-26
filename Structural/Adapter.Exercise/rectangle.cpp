#include "rectangle.hpp"
#include "clone_factory.hpp"

namespace
{
    bool is_registered = Drawing::ShapeFactory::instance()
            .register_shape("Rectangle", new Drawing::Rectangle());
}
