#ifndef ADAPTER_HPP_
#define ADAPTER_HPP_

#include <iostream>

// "Target"
class Target
{
public:
    virtual void request() { std::cout << "Target::request()" << std::endl; };
    virtual ~Target() = default;
};

// "Adaptee"
class Adaptee
{
public:
    virtual void request()
	{
		std::cout << "Called specific_request()" << std::endl;
	}

    virtual ~Adaptee()
    {
    }
};

// "Adapter"
class ClassAdapter : public Target, private Adaptee
{
public:
    void request() override
    {
        Adaptee::request();
    }
};

// "Adapter"
class ObjectAdapter : public Target
{
private:
	Adaptee& adaptee_;
public:
	ObjectAdapter(Adaptee& adaptee) : adaptee_(adaptee)
	{
	}
	
    void request() override
	{
        adaptee_.request();
	}
};

#endif /*ADAPTER_HPP_*/
