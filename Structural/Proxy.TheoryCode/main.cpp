#include "proxy.hpp"

using namespace std;

class Client
{
    unique_ptr<Subject> subject_;
public:
    Client(unique_ptr<Subject> subject) : subject_{move(subject)}
	{
	}

	void use()
	{
        subject_->request1();
	}
};

int main()
{
    unique_ptr<Proxy> proxy { new Proxy };

    Client c {move(proxy)};

	std::cout << std::endl;

	c.use();
	c.use();

    SynchronizingProxy sync_subject;

    std::lock_guard<SynchronizingProxy> lk { sync_subject };
    sync_subject.request1();
    sync_subject.request2();
}
