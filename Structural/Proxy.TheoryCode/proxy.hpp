#ifndef PROXY_HPP_
#define PROXY_HPP_

#include <iostream>
#include <string>
#include <memory>
#include <mutex>

// "Subject" 
class Subject
{
public:
    virtual void request1() = 0;
    virtual void request2() = 0;
    virtual ~Subject() = default;
};

// "RealSubject"
class RealSubject : public Subject
{
public:
	RealSubject()
	{
		std::cout << "RealSubject's creation" << std::endl;
	}

    RealSubject(const RealSubject&) = delete;
    RealSubject& operator=(const RealSubject&) = delete;

	~RealSubject()
	{
		std::cout << "RealSubject's clean-up" << std::endl;
	}
	
    void request1() override
	{
        std::cout << "Called RealSubject.request1()" << std::endl;
	}

    void request2() override
    {
        std::cout << "Called RealSubject.request2()" << std::endl;
    }
};

// "Proxy" 
class Proxy : public Subject
{
    std::unique_ptr<RealSubject> real_subject_;
    std::once_flag is_initialized_;
public:
    Proxy() : real_subject_{nullptr}
	{
		std::cout << "Proxy's creation" << std::endl;
	}

    void request1()
	{
		// lazy initialization'
        std::call_once(is_initialized_, [=] { real_subject_.reset(new RealSubject()); });

        real_subject_->request1();
	}

    void request2()
    {
        // lazy initialization'
        std::call_once(is_initialized_, [=] { real_subject_.reset(new RealSubject()); });

        real_subject_->request2();
    }
};

class SynchronizingProxy : public Subject
{
    RealSubject subject_;
    std::recursive_mutex mtx_;
public:
    void request1() override
    {
        std::lock_guard<std::recursive_mutex> lk {mtx_};
        subject_.request1();
    }

    void request2() override
    {
        std::lock_guard<std::recursive_mutex> lk {mtx_};
        subject_.request1();
    }

    void lock()
    {
        mtx_.lock();
    }

    void unlock()
    {
        mtx_.unlock();
    }
};

#endif /*PROXY_HPP_*/
