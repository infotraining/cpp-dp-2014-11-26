#include "shape_group.hpp"

namespace
{
    using namespace Drawing;
    bool is_registered =
            ShapeFactory::instance().
                register_shape("ShapeGroup", new ShapeGroup());
}
