#include <iostream>
#include <string>
#include <fstream>
#include <cassert>
#include <map>
#include <algorithm>
#include <stdexcept>

#include "shape.hpp"
#include "shape_group.hpp"
#include "clone_factory.hpp"
#include "application.hpp"

#include <boost/bind.hpp>

using namespace Drawing;
using namespace std;

class GraphicsDocument : public Document
{
    ShapeGroup shapes_;
	string title_;
public:
	virtual void open(const string& file_name)
	{
		title_ = file_name;

		ifstream fin(file_name.c_str());

        string type_identifier;
        fin >> type_identifier;
        assert(type_identifier == "ShapeGroup");

        shapes_.read(fin);

		cout << "Loading finished." << endl;
	}

	virtual void save(const std::string& file_name)
	{
		ofstream fout(file_name.c_str());
        shapes_.write(fout);
		fout.close();

		title_ = file_name;
	}

	virtual string get_title() const
	{
		return title_;
	}

	void show()
	{
		cout << "\n\nDrawing:\n";
        shapes_.draw();
	}
};

class GraphicsApplication : public Application
{
public:
	virtual Document* create_document()
	{
		return new GraphicsDocument();
	}
};

int main()
{
	GraphicsApplication app;

	app.open_document("drawing.txt");

	Document* ptr_doc = app.find_document("drawing.txt");

	ptr_doc->show();

	ptr_doc->save("new_drawing.txt");
}
