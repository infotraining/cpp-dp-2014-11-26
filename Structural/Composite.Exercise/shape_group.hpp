#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <list>
#include <memory>

namespace Drawing
{

using ShapePtr = std::shared_ptr<Shape>;

// TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
class ShapeGroup : public Shape
{
    std::list<ShapePtr> shapes_;
public:
    ShapeGroup() = default;

    ShapeGroup(const ShapeGroup& source)
    {
        for(const auto& shape : source.shapes_)
            shapes_.emplace_back(shape->clone());
    }

    void draw() const
    {
        for(const auto& shape : shapes_)
            shape->draw();
    }

    void move(int dx, int dy)
    {
        for(const auto& shape : shapes_)
            shape->move(dx, dy);
    }

    void read(std::istream &in)
    {
        int count;
        in >> count;

        std::string type_identifier;

        for(int i = 0; i < count; ++i)
        {
            in >> type_identifier;
            Shape* shape = ShapeFactory::instance().create(type_identifier);
            shape->read(in);
            shapes_.emplace_back(shape);
        }
    }

    void write(std::ostream &out)
    {
        out << "ShapeGroup " << shapes_.size() << std::endl;

        for(const auto& shape : shapes_)
            shape->write(out);
    }

    Shape *clone() const
    {
        return new ShapeGroup(*this);
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/
