#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <memory>
#include <functional>
#include <sstream>

const double normal_interest_rate = 0.05;
const double overdraft_interest_rate = 0.15;

struct AccountContext
{
    int id;
    double balance;
};

class AccountState
{
public:
    virtual AccountState* withdraw(AccountContext& context, double amount) = 0;
    virtual AccountState* deposit(AccountContext& context, double amount) = 0;
    virtual AccountState* pay_interest(AccountContext& context) = 0;
    virtual std::string status() = 0;
    virtual ~AccountState() = default;
};

class AccountStateBase : public AccountState
{
    double interest_;
public:
    AccountStateBase(double interest) : interest_{interest}
    {}

    AccountState* pay_interest(AccountContext& context) override
    {
        context.balance += context.balance * interest_;

        return this;
    }
};

class NormalState : public AccountStateBase
{
public:
    NormalState(double interest = normal_interest_rate) : AccountStateBase{interest}
    {}

    AccountState* withdraw(AccountContext &context, double amount);

    AccountState* deposit(AccountContext &context, double amount)
    {
        context.balance += amount;

        return this;
    }

    std::string status()
    {
        return "Normal";
    }
};

class OverdraftState : public AccountStateBase
{
public:
    OverdraftState(double interest = overdraft_interest_rate) : AccountStateBase{interest}
    {}

    AccountState* withdraw(AccountContext &context, double amount);

    AccountState* deposit(AccountContext &context, double amount);

    std::string status()
    {
        return "Overdraft";
    }
};

class BankAccount
{
    AccountContext context_ = {};
    AccountState* state_;

public:
    static NormalState normal_state;
    static OverdraftState overdraft_state;

    BankAccount(int id) : context_ {id, 0.0}, state_(&BankAccount::normal_state) {}

	void withdraw(double amount)
	{
		assert(amount > 0);

        state_ = state_->withdraw(context_, amount);
	}

	void deposit(double amount)
	{
		assert(amount > 0);

        state_ = state_->deposit(context_, amount);
	}

	void pay_interest()
	{
        state_ = state_->pay_interest(context_);
	}

    std::string status() const
	{
        std::stringstream strm;
        strm << "BankAccount #" << context_.id
             << "; State: " << state_->status()
             << "; Balance: " << context_.balance;

        return strm.str();
	}

	double balance() const
	{
        return context_.balance;
	}

	int id() const
	{
        return context_.id;
	}
};

#endif
