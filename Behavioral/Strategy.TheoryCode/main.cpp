#include "strategy.hpp"

int main()
{
    Context context { std::make_shared<ConcreteStrategyA>() };
    context.context_interface();

    auto s2 = std::make_shared<ConcreteStrategyB>();
    context.reset_strategy(s2);
    context.context_interface();

    context.reset_strategy(std::make_shared<ConcreteStrategyC>());
    context.context_interface();
}
