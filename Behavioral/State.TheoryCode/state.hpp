#ifndef STATE_HPP_
#define STATE_HPP_

#include <iostream>
#include <string>
#include <typeinfo>

class Context;

// "State"
class State
{
public:
    virtual State* handle(Context* context) = 0;
	virtual ~State() {}
};

// "ConcreteStateA"
class ConcreteStateA : public State
{
public:
    State* handle(Context* context);
};


// "ConcreteStateB"
class ConcreteStateB : public State
{
public:
    State* handle(Context* context);
};


// "Context"
class Context
{
	State* state_;

	Context(const Context&);
	Context& operator=(const Context&);
public:
	Context(State* state) : state_(state)
	{
	}

	State* get_state()
	{
		return state_;
	}

	void request()
	{
        set_state(state_->handle(this));
	}

	~Context()
	{
		delete state_;
	}
 private:
    void set_state(State* state)
    {
        delete state_;
        state_ = state;

        std::cout << "State: " << typeid(*state_).name() << std::endl;
    }
};

State* ConcreteStateA::handle(Context* context)
{
    return new ConcreteStateB();
}

State* ConcreteStateB::handle(Context* context)
{
    return new ConcreteStateA();
}

#endif /*STATE_HPP_*/
